# Autentificación con Facebook en Auth0 para Ionic
Con la Ayuda de Auth0 podemos conectarnos con diferentes redes sociales, en este caso vamos a realizar con Facebook,

Para ello se debe tener Creado una aplicación en Auth0 de tipo **native**

![imagen](imagenes/app.png)

## Conectarse con Facebook

* Para ello debemos dirigirnos a **Connections** a lado izquierdo, seleccionamos **Social** y nos aparecerá las diferentes redes sociales con las cuales podemos conectarnos, seleccionamos **Facebook**.

![imagen](imagenes/social-fb.png)

* Nota: Para el uso de Facebook, es necesario crear una aplicación en Facebook Developers, ya que se debe obtener los siguientes campos para usar la autenticación con Facebook.

![imagen](imagenes/app-ID.png)

## Crear Aplicación en Facebook Developers
* Nos dirigimos a la página oficial de [Facebook Developers](https://developers.facebook.com/)

![imagen](imagenes/fb.png)

* Nos dirigimos a **My Apps** y seleccionamos **add new app**, y creamos la nueva app.

![imagen](imagenes/fb-create.png)

* Se debe colocar un nombre para la aplicación

![imagen](imagenes/fb-name.png)

* Como solo se usará para un login, solo se seleccionará **Integrate Facebook Login**

![imagen](imagenes/fb-login.png)

* Añadimos el callback url, dentro **Facebook Login** y en **Setting** en el campo **Valid OAuth Redirect URIs**, se debe cambiar YOUR_DOMAIN 
```
https://<YOUR_DOMAIN>/login/callback
```
![imagen](imagenes/fb-url.png)

* Se debe copiar los siguientes campos:
* **APP ID**
* **App Secret**

![imagen](imagenes/fb-data.png)

* Se deben copiar dentro de la configuración de la red social del **Auth0**

![imagen](imagenes/fb-secret.png)

## Uso en Ionic
* Para poder usar es necesario tener creado un método el cual nos ayude a logearnos:

```javascript
login(nombreRedSocial: string) {
        const options = {
            scope: 'openid profile offline_access',
            connection: nombreRedSocial
        };

        this.auth0Cordova.authorize(options, (err, auth0Result) => {
            if (err) {
                throw err;
            }

            this.setIdToken(auth0Result.idToken);
            this.setAccessToken(auth0Result.accessToken);
            const expiresAt = JSON.stringify((auth0Result.expiresIn * 1000) + new Date().getTime());
            this.setStorageVariable('expires_at', expiresAt);
            this.obtenerInformacionDelUsuarioLogeado(this.accessToken)
        });
    }
```
* recibe como parámetro el nombre de la red social con la cual nos estamos logeando, para ver el nombre debemos regresar al **Auth0** dentro de **Connections** -> **Social**, damos click a **Facebook**, el atributo **Name** nos indica el nombre.

![imagen](imagenes/fb-app-name.png),

* Después se crea un botón con este método y con su respectivo nombre de la red social.

```javascript
// ts
loginConFacebook() {
    this._authService.login('facebook');
  }
```
```html
<!--html-->
<button ion-button icon-left class="button button-block" (click)="loginConFacebook()">
              <ion-icon name="logo-facebook"> </ion-icon>
              sign up with facebook
            </button>
```
<a href="https://twitter.com/kevi_n_24" target="_blank"><img alt="Sígueme en Twitter" height="35" width="35" src="https://3.bp.blogspot.com/-E4jytrbmLbY/XCrI2Xd_hUI/AAAAAAAAIAo/qXt-bJg1UpMZmTjCJymxWEOGXWEQ2mv3ACLcBGAs/s1600/twitter.png" title="Sígueme en Twitter"/> @kevi_n_24 </a><br>

